# Atomic CMS documentation

Welcome to the online documentation of the content management system "Atomic CMS"!
The Atomic CMS based on PHP7 and support various databases like MySQL, Oracle and more. It's an ambitious project started by two young developers who think, that there is currently no cms with a modern code base and features.

## Getting started

### System structure

Understand how the whole system works is essential to work in a great way on this project. So here you find the system structure: [system structure](System_structure.md)

### Modules

All modules of the system core get their own section, so you could easily find any information you need:

- [Autoload](Modules/Autoloading.md)

### Contribute

At first the most relevant link for new contributor: [Contribution workflow](Contribution_Workflow.md)
Please read the article carefully, because an invalid commit message or a wrong function comment will prevent a merge!