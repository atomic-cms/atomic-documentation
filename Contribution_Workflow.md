# How to contribute

At first: Thank you for your help!

## Language

The language of comments, documentation, conversation etc. is english!

## Tasks

If you want to contribute, you have to work with tasks, which are located in our ticket system http://issues.atomic-cms.com.
Conclusion: Work on ticket or your pull request will not be merged!

### Redmine

Currently the redmine is closed for registrations!

### Documentation

To each ticket belongs two pull requests!

- The pull request with the code for the cms
- The pull request with the documentation

If you don't write a good documentation, the pull request will not be merged!

## Coding guidelines

We work with an advanced PSR-2 standard. This means, the complete PSR-2 standard is supported, but we have some additional features.

[PSR-2 coding style guide](https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-2-coding-style-guide.md)

### Additions

#### Function parameter declaration

A function head requires a type hinting for all of the parameters, no matter if the parameter is an object or an scalar.

Example:
```PHP
class someClass
{

    public function example(int $integer, string $string, array $array, \DateTime $dateTime){
        // do some magic
    }

}
```

#### Function return value declaration

All functions must declare the return value.

Example:
```PHP
class someClass
{

    public function example():int {
        // do some magic
        return 1;
    }

}
```


#### Early returns

Early returns are forbidden! No discussion or something, just don't do it!

## GIT

### Commit messages

A commit message for the Atomic CMS must be in the following form:

```
[Redmine project shorty]-[Redmine ticket number without #]:
Description: Description of the ticket
Author: Your name <your@email.com>
Conclusion: Conclusion of what you done
```

Remember: You have to do min. two commits:
1. Commit your code
2. Commit the documentation for your code

### Pull requests

You never ever merge your own pull request, even if you could this!

### QA

Every pull request must be confirmed by two peoples, who can merge!


## File structure

### Head comment

Each file in this project need a head comment. The comment have to be in the following form:

```
/***********************************************************************************************************************
 * Vendor                   : Atomic CMS
 * Editors                  : Your name <your@email.com>
 *                            Another name <another@email.com>
 * URL                      : http://atomic-cms.com
 * Issues                   : http://issues.atomic-cms.com
 * Documentation            : http://docs.atomic-cms.com
 * License                  : TBD
 **********************************************************************************************************************/
```